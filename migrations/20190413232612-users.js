'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  db.createTable('users', {
    id: {
      type: 'int',
      primaryKey: true,
      autoIncrement: true
    },
    full_name: {
      type: 'string',
      length: 255
    },
    email: {
      type: 'string',
      length: 255
    },
    password: {
      type: 'string',
      length: 255
    },
    role: {
      type: 'string',
      length: 13
    },
    created_at: {
      type: 'string',
      length: 64
    },
    created_by: {
      type: 'int',
    },
    modified_at: {
      type: 'string',
      length: 64
    },
    modified_by: {
      type: 'int',
    },
    activation_code: {
      type: 'string',
      length: 255
    },
    active: {
      type: 'boolean'
    }
  }, (err) => {
    if (err) return callback(err);
    return callback()
  })
};

exports.down = function(db) {
  return null;
};

exports._meta = {
  "version": 1
};
