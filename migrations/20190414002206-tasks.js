'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  db.createTable('tasks', {
    id: {
      type: 'int',
      primaryKey: true,
      autoIncrement: true
    },
    task: {
      type: 'text'
    },
    owner: {
      type: 'int'
    },
    started_at: {
      type: 'string',
      length: 64,
    },
    finished_at: {
      type: 'string',
      length: 64
    },
    time_expended: {
      type: 'int'
    },
    created_by: {
      type: 'int'
    },
    created_at: {
      type: 'string',
      length: 64
    },
    modified_by: {
      type: 'int'
    },
    modified_at: {
      type: 'string',
      length: 64
    }
  }, (err) => {
    if (err) return callback(err);
    return callback(err);
  })
};

exports.down = function(db) {
  return null;
};

exports._meta = {
  "version": 1
};
