const bcrypt = require('bcrypt');

const checkHash = (string, hash) => bcrypt.compareSync(string, hash);

module.exports = checkHash;