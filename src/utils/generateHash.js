const bcrypt = require('bcrypt');
const salt = bcrypt.genSaltSync(10);

const generateHash = string => (bcrypt.hashSync(string, salt));

module.exports = generateHash;